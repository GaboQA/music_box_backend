<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('archives', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('from');
            $table->string('name');
            $table->string('extend');
            $table->string('to')->nullable();
            $table->string('status')->default('Pending');
            $table->string('verifysum')->nullable();
            $table->timestamps();
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archives');
    }
}
