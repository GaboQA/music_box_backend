<br>
<br>
<br>
<div class="row">
  <div class="col-sm-6 col-xs-6 col-xxs-12">
    <div class="urlconverter-section-3-left">
      <div class="title">
        <h3>Instructions</h3>
        <span class="title-border"></span>
      </div>
      <ol>
        <li>Choose the audio that you wish to convert.</li>
        <li>Select a format of your choice.</li>
        <li>Click the "Start" button to begin the conversion process.</li>
        <li>Upon successful completion of the conversion, you will receive a download link for the converted file.</li>
      </ol>
    </div>
  </div>

  <div class="col-sm-6 col-xs-6 col-xxs-12">
    <div class="urlconverter-section-3-right">
      <div class="title">
        <h3>Features</h3>
        <span class="title-border"></span>
      </div>
      <ul>
        <li><span class="arrow"></span>Full compatibility with modern browsers</li>
        <li><span class="arrow"></span>High-speed conversions</li>
        <li><span class="arrow"></span>No registration required</li>
        <li><span class="arrow"></span>Unlimited free conversions and downloads</li>
        <li><span class="arrow"></span>No software installation necessary</li>
      </ul>
    </div>
  </div>
</div>
<br>
<br>
<footer class="d-flex flex-column  bd-highlight mb-3">
    <div class="align-self-center">
      <p>Music Box Converter &copy;  <?php echo date("Y"); ?></p>
      <p><a href="#">Logical and Support</a></p>
    </div>
</footer>
</div>
<!-- Scripts -->

</body>

</html>