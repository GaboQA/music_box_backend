<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Archive;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class ConvertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $archive = Archive::all()->toArray();
        return response()->json($archive);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // save archive in storage
        $newpath = $request->audio
        ->storeAs('upload', $request->audio
        ->getClientOriginalName());

        $from = storage_path().'/app/upload/'.$request->audio->getClientOriginalName();

        $CheckSum = md5_file($from);
        $audioinfo = Archive::where('verifysum','=', $CheckSum)
        ->where('to','=', $request->to)
        ->first();
        
        //check if verifysum is similar in 
        if ($audioinfo === null) {

            // get data archive to set in database
            $request['from'] = $from;
            $request['name'] = $request->audio->getClientOriginalName();
            $request['extend']  = pathinfo($request->audio
            ->getClientOriginalName(), PATHINFO_EXTENSION);

            //set data archive in database
            $audioinfo = Archive::create($request->all());
        }
        
        //<---send msg to rabbit---->

        //create a array with audio info
        $json = array('id'=> $audioinfo->id,
        'from'=> $from,
        'to'=> $request->to,
        'name'=>pathinfo($audioinfo->from, PATHINFO_FILENAME));

        $rabbitmsg = json_encode($json);

        $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
        $channel = $connection->channel();

        $channel->queue_declare('music_box_msg', false, false, false, false);

        //declare message
        $msg = new AMQPMessage($rabbitmsg);
        $channel->basic_publish($msg, '', 'music_box_msg');


        $channel->close();
        $connection->close();
        
        //return audio information
        return response()->json($audioinfo, 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
