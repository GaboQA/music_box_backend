<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Archive;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Session;
use Illuminate\Support\Facades\Cache;


class DownloadController extends Controller
{
    public function index()
    {
        return Storage::download('/a.avi');
        $archive = Archive::all()->toArray();
        return response()->json($archive);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Here  Download archive with parameters(Route this archive and name).
    public function store(Request $request)
    {   
        //give id this archive
        //Concatain router
        $id_audio=$request->audio_id;
        $id_audio = (int) $id_audio;
        //return response()->json($id_audio, 201);
        //Query name in this database
        $audio_info = Archive::all()->where('id','=',$id_audio);
        //return response()->json('El audio se descargo '. $audio_info, 201);
        //Travel array with foreach
        foreach ($audio_info as $i => $value) {            
              $Name_archive=$value->name;
        }  
        //return response()->json("/a.avi");

        redirect('http://127.0.0.1:8001/down');
        
         //Cache::put('key', $Name_archive);
         $Retorno=Storage::download('/a.avi');
        return response()->json("hello word", 201);
        //header('Location: https://stackoverflow.com');    
    }
    public function download($id){
        /*$value = Cache::get('key');
        return response()->json($value);*/

         //Query name in this database
         $audio_info = Archive::all()->where('id','=',$id);
         //return response()->json('El audio se descargo '. $audio_info, 201);
         //Travel array with foreach
         foreach ($audio_info as $i => $value) {            
               $Name_archive=$value->name;
         }  
        return Storage::download('download/'.$Name_archive);
    }
}
