<?php

namespace App\Console\Commands;

use PhpAmqpLib\Connection\AMQPStreamConnection; 
use PhpAmqpLib\Message\AMQPMessage;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use App\Archive;

class Cronjob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:execute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CronJob to update information';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    //Here will be the code that recive
    $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
    $channel = $connection->channel();
    $channel->queue_declare('hello', false, false, false, false);

    echo " [*] Waiting for messages. To exit press CTRL+C\n";

    $callback = function ($msg) {

        $msg_recive = json_decode($msg->body,true);

        //get the route to donwload in server
        $Route =' '.storage_path().'/app/download/';
        
        //optain sum verification
        $from = $msg_recive['from'];
        $CheckSum = md5_file($from);
        //find coincidences
        $audioinfo = Archive::where('verifysum','=', $CheckSum)
        ->where('to','=', $msg_recive['to'])
        ->first();

        //is audioinfo is null we convert the audio
        if ($audioinfo === null) {
            try {
                //we concatenate $Route 
                $output = shell_exec('ffmpeg -i '.$msg_recive['from']
                .$Route.$msg_recive['name'].'.'.$msg_recive['to']);
    
                echo "Succes file is ready";
    
            } catch (\Throwable $th) {
                echo " Error the file is damaged";
            }
        }

        //update url to dowload file
        Archive::where('id', '=', $msg_recive['id'])
        ->update(array('from' => $Route.$msg_recive['name'].'.'.$msg_recive['to'],
        'name' => $msg_recive['name'].'.'.$msg_recive['to'],
        'verifysum'=>$CheckSum, 
        'status'=>'Ready'));       
    };
    
    $channel->basic_consume('music_box_msg', '', false, true, false, false, $callback);
    
    while (count($channel->callbacks)) { 
        $channel->wait();
    }

    }
}

