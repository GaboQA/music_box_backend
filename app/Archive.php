<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archive extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','from','name','extend','to','status','verifysum'
    ];
}
